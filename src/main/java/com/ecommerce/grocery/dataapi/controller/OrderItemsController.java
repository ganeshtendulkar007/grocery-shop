package com.ecommerce.grocery.dataapi.controller;

import com.ecommerce.grocery.dataapi.service.OrderItemsService;
import com.ecommerce.grocery.dataapi.dto.OrderItemsDTO;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/orderItems")
@Slf4j
@SecurityRequirement(name = "rest-api")
public class OrderItemsController {

    @Autowired
    private OrderItemsService orderItemsService;

    @PostMapping
    public ResponseEntity<Object> save(@Valid @RequestBody List<OrderItemsDTO> orderItemsDTOS) {
        try {
            orderItemsService.save(orderItemsDTOS);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            orderItemsService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            OrderItemsDTO orderItemsDTO = orderItemsService.getById(id);
            return ResponseEntity.ok().body(orderItemsDTO);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }
}

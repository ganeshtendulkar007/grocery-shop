package com.ecommerce.grocery.dataapi.controller;

import com.ecommerce.grocery.dataapi.service.ProductsService;
import com.ecommerce.grocery.dataapi.dto.ProductsDTO;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/products")
@Slf4j
@SecurityRequirement(name = "rest-api")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @PostMapping
    public ResponseEntity<Object> save(@Valid @RequestBody List<ProductsDTO> productsDTOS) {
        try {
            productsService.save(productsDTOS);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            productsService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            ProductsDTO productsDTO = productsService.getById(id);
            return ResponseEntity.ok().body(productsDTO);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }
}

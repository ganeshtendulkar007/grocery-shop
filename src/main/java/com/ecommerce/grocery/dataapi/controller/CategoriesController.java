package com.ecommerce.grocery.dataapi.controller;

import com.ecommerce.grocery.dataapi.service.CategoriesService;
import com.ecommerce.grocery.dataapi.dto.CategoriesDTO;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/categories")
@Slf4j
@SecurityRequirement(name = "rest-api")
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    @PostMapping
    public ResponseEntity<Object> save(@Valid @RequestBody List<CategoriesDTO> categoriesDTOS) {
        try {
            categoriesService.save(categoriesDTOS);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            categoriesService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@Valid @NotNull @PathVariable("id") Long id) {
        try {
            CategoriesDTO categoriesDTO = categoriesService.getById(id);
            return ResponseEntity.ok().body(categoriesDTO);
        } catch (Exception e){
            log.error(e.toString());
            return ResponseEntity.internalServerError().build();
        }
    }
}

package com.ecommerce.grocery.dataapi.repository;

import com.ecommerce.grocery.dataapi.entity.OrderItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrderItemsRepository extends JpaRepository<OrderItems, Long>, JpaSpecificationExecutor<OrderItems> {

}
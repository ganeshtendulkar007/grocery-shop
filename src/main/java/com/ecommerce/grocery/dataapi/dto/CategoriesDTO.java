package com.ecommerce.grocery.dataapi.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class CategoriesDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long categoryId;
    private String name;
    private String description;

}

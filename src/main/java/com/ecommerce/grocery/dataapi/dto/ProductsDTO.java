package com.ecommerce.grocery.dataapi.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ProductsDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long productId;
    private String name;
    private String description;
    private BigDecimal price;
    private Long stockQuantity;
    private Long categoryId;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}

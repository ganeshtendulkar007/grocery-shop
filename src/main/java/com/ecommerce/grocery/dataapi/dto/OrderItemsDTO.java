package com.ecommerce.grocery.dataapi.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderItemsDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long orderItemId;
    private Long orderId;
    private Long productId;
    private Long quantity;
    private BigDecimal price;
    private BigDecimal totalPrice;

}

package com.ecommerce.grocery.dataapi.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CustomersDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long customerId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String shippingAddress;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}

package com.ecommerce.grocery.dataapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    private static final String[] URIs = {"/actuator/**", "/swagger-ui.html", "/swagger-ui/**",
            "/swagger-resources/**", "/v3/api-docs", "/v3/api-docs/**"};

    private static final String scope = "scope:eccomerce-data-api-modify";

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity.authorizeHttpRequests(auth -> {
                auth.requestMatchers(URIs).permitAll();
                auth.requestMatchers("/**").hasAnyAuthority("SCOPE_"+scope);
                })
                .oauth2ResourceServer(oAuth2 -> oAuth2.jwt(Customizer.withDefaults()))
                .csrf(AbstractHttpConfigurer::disable)
                .build();
    }
}

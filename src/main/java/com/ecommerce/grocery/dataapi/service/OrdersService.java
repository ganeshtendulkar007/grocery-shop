package com.ecommerce.grocery.dataapi.service;

import com.ecommerce.grocery.dataapi.dto.OrdersDTO;
import com.ecommerce.grocery.dataapi.entity.Orders;
import com.ecommerce.grocery.dataapi.repository.OrdersRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    public void save(List<OrdersDTO> ordersDTOS) {
        List<Orders> ordersList = new ArrayList<>();
        ordersDTOS.forEach(ordersDTO -> {
            Orders orders = dtoToEntity(ordersDTO);
            ordersList.add(orders);
        });
        ordersRepository.saveAllAndFlush(ordersList);
    }

    public Orders dtoToEntity(OrdersDTO ordersDTO) {
        Orders orders = new Orders();
        BeanUtils.copyProperties(ordersDTO, orders);
        return orders;
    }

    public void delete(Long id) {
        ordersRepository.deleteById(id);
    }

    public OrdersDTO getById(Long id) {
        Orders orders = findById(id);
        return entityToDTO(orders);
    }

    private OrdersDTO entityToDTO(Orders orders) {
        OrdersDTO ordersDTO = new OrdersDTO();
        BeanUtils.copyProperties(orders, ordersDTO);
        return ordersDTO;
    }

    private Orders findById(Long id) {
        return ordersRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}

package com.ecommerce.grocery.dataapi.service;

import com.ecommerce.grocery.dataapi.repository.ProductsRepository;
import com.ecommerce.grocery.dataapi.dto.ProductsDTO;
import com.ecommerce.grocery.dataapi.entity.Products;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    public void save(List<ProductsDTO> productsDTOS) {
        List<Products> productsList = new ArrayList<>();
        productsDTOS.forEach(productsDTO -> {
            Products products = dtoToEntity(productsDTO);
            productsList.add(products);
        });
        productsRepository.saveAllAndFlush(productsList);
    }

    public Products dtoToEntity(ProductsDTO productsDTO) {
        Products products = new Products();
        BeanUtils.copyProperties(productsDTO, products);
        return products;
    }

    public void delete(Long id) {
        productsRepository.deleteById(id);
    }

    public ProductsDTO getById(Long id) {
        Products products = findById(id);
        return entityToDTO(products);
    }

    private ProductsDTO entityToDTO(Products products) {
        ProductsDTO productsDTO = new ProductsDTO();
        BeanUtils.copyProperties(products, productsDTO);
        return productsDTO;
    }

    private Products findById(Long id) {
        return productsRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}

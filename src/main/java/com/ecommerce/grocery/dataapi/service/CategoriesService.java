package com.ecommerce.grocery.dataapi.service;

import com.ecommerce.grocery.dataapi.dto.CategoriesDTO;
import com.ecommerce.grocery.dataapi.entity.Categories;
import com.ecommerce.grocery.dataapi.repository.CategoriesRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CategoriesService {

    @Autowired
    private CategoriesRepository categoriesRepository;

    public void save(List<CategoriesDTO> categoriesDTOS) {
        List<Categories> categoriesList = new ArrayList<>();
        categoriesDTOS.forEach(categoriesDTO -> {
            Categories categories = dtoToEntity(categoriesDTO);
            categoriesList.add(categories);
        });
        categoriesRepository.saveAllAndFlush(categoriesList);
    }

    public Categories dtoToEntity(CategoriesDTO categoriesDTO) {
        Categories categories = new Categories();
        BeanUtils.copyProperties(categoriesDTO, categories);
        return categories;
    }

    public void delete(Long id) {
        categoriesRepository.deleteById(id);
    }

    public CategoriesDTO getById(Long id) {
        Categories categories = findById(id);
        return entityToDTO(categories);
    }

    private CategoriesDTO entityToDTO(Categories categories) {
        CategoriesDTO categoriesDTO = new CategoriesDTO();
        BeanUtils.copyProperties(categories, categoriesDTO);
        return categoriesDTO;
    }

    private Categories findById(Long id) {
        return categoriesRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}

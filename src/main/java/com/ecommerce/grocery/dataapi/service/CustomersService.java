package com.ecommerce.grocery.dataapi.service;

import com.ecommerce.grocery.dataapi.dto.CustomersDTO;
import com.ecommerce.grocery.dataapi.entity.Customers;
import com.ecommerce.grocery.dataapi.repository.CustomersRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CustomersService {

    @Autowired
    private CustomersRepository customersRepository;

    public void save(List<CustomersDTO> customersDTOS) {
        List<Customers> customersList = new ArrayList<>();
        customersDTOS.forEach(customersDTO -> {
            Customers customers = dtoToEntity(customersDTO);
            customersList.add(customers);
        });
        customersRepository.saveAllAndFlush(customersList);
    }

    public Customers dtoToEntity(CustomersDTO customersDTO) {
        Customers customers = new Customers();
        BeanUtils.copyProperties(customersDTO, customers);
        return customers;
    }

    public void delete(Long id) {
        customersRepository.deleteById(id);
    }

    public CustomersDTO getById(Long id) {
        Customers customers = findById(id);
        return entityToDTO(customers);
    }

    private CustomersDTO entityToDTO(Customers customers) {
        CustomersDTO customersDTO = new CustomersDTO();
        BeanUtils.copyProperties(customers, customersDTO);
        return customersDTO;
    }

    private Customers findById(Long id) {
        return customersRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}

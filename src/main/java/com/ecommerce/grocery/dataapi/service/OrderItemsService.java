package com.ecommerce.grocery.dataapi.service;

import com.ecommerce.grocery.dataapi.dto.OrderItemsDTO;
import com.ecommerce.grocery.dataapi.entity.OrderItems;
import com.ecommerce.grocery.dataapi.repository.OrderItemsRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OrderItemsService {

    @Autowired
    private OrderItemsRepository orderItemsRepository;

    public void save(List<OrderItemsDTO> orderItemsDTOS) {
        List<OrderItems> orderItemsList = new ArrayList<>();
        orderItemsDTOS.forEach(orderItemsDTO -> {
            OrderItems orderItems = dtoToEntity(orderItemsDTO);
            orderItemsList.add(orderItems);
        });
        orderItemsRepository.saveAllAndFlush(orderItemsList);
    }

    public OrderItems dtoToEntity(OrderItemsDTO orderItemsDTO) {
        OrderItems orderItems = new OrderItems();
        BeanUtils.copyProperties(orderItemsDTO, orderItems);
        return orderItems;
    }

    public void delete(Long id) {
        orderItemsRepository.deleteById(id);
    }

    public OrderItemsDTO getById(Long id) {
        OrderItems orderItems = findById(id);
        return entityToDTO(orderItems);
    }

    private OrderItemsDTO entityToDTO(OrderItems orderItems) {
        OrderItemsDTO orderItemsDTO = new OrderItemsDTO();
        BeanUtils.copyProperties(orderItems, orderItemsDTO);
        return orderItemsDTO;
    }

    private OrderItems findById(Long id) {
        return orderItemsRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}
